from azure.servicebus import ServiceBusClient


class ConfigServiceBus:


    def __init__(self):
        self.CONNECTION_STR = "<REPLACE YOUR CONNECTION>"
        self.client = ServiceBusClient.from_connection_string(self.CONNECTION_STR)



    def get_client(self):
        return self.client
