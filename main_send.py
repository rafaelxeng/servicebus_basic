from Remetente import Remetente
from datetime import datetime

if __name__ == '__main__':
    remetente = Remetente('diagnostico_fiscal')

    status = remetente.enviar_mensagem({'data':datetime.now()})
    print(status)